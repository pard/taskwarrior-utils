import csv
from dateutil.parser import parse as parse_date
from dateutil.relativedelta import relativedelta
import sys
from tasklib import TaskWarrior, Task


def get_csv(file):
    with open(file, newline='') as f:
        return list(csv.reader(f))


def parse_csv(data):
    header = data.pop(0)
    to_import = []
    for entry in data:
        entry_dict = {}
        for index, column in enumerate(entry):
            entry_dict[header[index]] = column
        to_import.append(entry_dict)
    return to_import


def import_tasks(to_import):
    tw = TaskWarrior('~/.task')
    for entry in to_import:
        date = parse_date(entry['date'])
        task = Task(tw, description=entry['description'],
                    due=date+relativedelta(days=1),
                    project="tri",
                    tags=['@gym', 'exercise'],
                    wait=date
                    )
        task.save()
    return "results", "err"


def main(file):
    data = get_csv(file)
    to_import = parse_csv(data)
    results, err = import_tasks(to_import)


if __name__ == "__main__":
    file = sys.argv[1]
    main(file)
